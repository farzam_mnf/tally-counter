package main.java.sbu.cs;

public class ExerciseLecture5 {

    /*
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     */
    public static String weakPassword(int length) {
        String pas = "";
        int max = 122 , min = 97 , d = 0 ;
        for(int i = 0 ; i < length ; i++){
            d = (int)(Math.random() * (max - min + 1) + min);
            pas = pas + (char) d ;
        }
        return pas;
    }

    /*
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     */
    public static String strongPassword(int length) throws  Exception  {
        String pas = "";
        int max = 122 , min = 97 , d = 0 , t2 = 0 ;
        if(length < 3 ){
            throw new Exception()  ;
        }
        char[] s = {'?','@','!','<','>','#','%','^','(',')','%','*','_','-','.','/','='};
        int t = (int)(Math.random() * (17) );
        pas = pas + s[t];
        int t1 = (int)(Math.random() * (10) + 48) ;
        pas = pas + (char) t1 ;
        for(int i = 3 ; i <= length ; i++){
            t2 = (int)(Math.random() * (3) + 1);
            if(t2 == 1){
                d = (int)(Math.random() * (max - min + 1) + min);
                pas = pas + (char) d ;
            }
            if(t2 == 2){
                t = (int)(Math.random() * (17) );
                pas = pas + s[t];
            }
            if(t2 == 3){
                t1 = (int)(Math.random() * (10) + 48) ;
                pas = pas + (char) t1 ;
            }
        }
        return pas;
    }

    /*
     *   implement a function that checks if a integer is a fibobin number
     *   integer n is fibobin is there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     */
    public static boolean isFiboBin(int n) {
        int a =1 , b = 1 , m = 0 , b1 = 0;
        while( m + b1 <= n ){
            int temp = 0 ;
            temp = b ;
            b = a + b ;
            a = temp ;
            b1 = b ;
            while(b > 1){
                int t = b % 2 ;
                if(t == 1){
                    b++ ;
                }
                b = b / 2 ;
            }
            if( b == 1 ){
                m++ ;
            }
            if(m + b1 == n){
                return false ;
            }
        }
        return true;
    }
}

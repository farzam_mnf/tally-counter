package main.java.sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6 {

    /*
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     */
    public static long calculateEvenSum(int[] arr) {
        long n = 0 ;
        for(int i = 0 ; i < arr.length ; i = i + 2 ){
            n = n + arr[i] ;
        }
        return n;
    }

    /*
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     */
    public static int[] reverseArray(int[] arr) {
        int t = arr.length - 1 ;
        for(int i = 0 ; i < arr.length / 2 ; i++ ){
            int temp = arr[i] ;
            arr[i] = arr[t] ;
            arr[t] = temp ;
            t-- ;
        }
        return arr;
    }

    /*
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     */
    public static double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException {
        int a1 = m1.length;
        int a2 = m1[0].length;
        int b1 = m2.length;
        int b2 = m2[0].length;
        double[][] m3 = new double[a1][b2];
        if(a2 != b1 ){
            throw new RuntimeException();
        }
        for(int i = 0 ; i < a1 ; i++ ){
            for(int t = 0 ; t < b2 ; t++ ) {
                for (int j = 0; j < a2; j++) {
                    m3[i][t] = m1[i][j] * m2[j][t] + m3[i][t];
                }
            }
        }
        return m3;
    }

    /*
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     */
    public List<List<String>> arrayToList(String[][] names) {
        return null;
    }

    /*
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     */
    public static List<Integer> primeFactors(int n) {
        List<Integer> prime = new ArrayList<>();
        int x=0;
        int y=0;
        for (int i=2 ; i<=n ; i++ ) {
            if (n%i==0) {
                n = n/i;
                for (int j = 0; i < prime.size(); j++) {
                    if (i == prime.get(j)) {
                        y++;
                    }
                }
                if (i%2==0 && i!=2) {
                    y++;
                }
                if (y==0) {
                    prime.add(x,i);
                    x++;
                    i=2;
                }
                y=0;
            }

        }
        if (x==0 && n!=1) {
            prime.add(0,n);
        }

        return prime;
    }

    /*
     *   implement a function that return a list of words in a given string
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line) {
//        public static List<String> extractWord(String line) {
//            List<String> a = new ArrayList<>();
//            String[] aa = line.split(" ");
//            for (int i=0 ; i<aa.length ; i++) {
//                a.add(i,aa[i]);
//            }
//            String sb = null;
//
//            for (int i=0 ; i<aa.length ; i++) {
//                sb = a.get(i);
//                StringBuilder str = new StringBuilder(sb);
//                for (int j=0 ; j<str.length() ; j++) {
//                    if (str.charAt(j) == '?' || str.charAt(j) == 'i' || str.charAt(j) == ',' ) {
//                        str.deleteCharAt(j);
//                    }
//                }
//                a.add(i,str.toString());
//                sb = null;
//                str= null;
//
//            }
//            return a;
//        }

        return null;
    }
}

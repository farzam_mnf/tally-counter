package main.java.sbu.cs;

public class ExerciseLecture4 {

    /*
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     */
    public long factorial(int n) {
        int counter = 1;
        for (int i = 1; i <= n; i++) {
            counter *= i;
        }
        return counter;
    }

    /*
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     */
    public long fibonacci(int n) {
        long num[]=new long[n+2];
        num[1]=1;
        num[2]=1;
        for (int i=3 ; i<= n ; i++){
            num[i] = num[i-1] + num[i-2];
        }

        return num[n];
    }

    /*
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     */
    public String reverse(String word) {
        StringBuilder input1 = new StringBuilder();
        input1.append(word);
        return String.valueOf(input1.reverse());
    }

    /*
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     */
    public boolean isPalindrome(String line) {
        String[] part1 = line.split(" ");
        int n = 0 ;
        String word = "" ;
        for(int i = 0 ; i < part1.length ; i++ ) {
            word = word + part1[i] ;
        }
        System.out.println(word);
        String[] part2 = word.split("");
        int j = part2.length - 1 ;
        for(int i = 0 ; i <= part2.length/2 ; i++ ){
            if(part2[j].equals(part2[i]) != true && part2[i].toUpperCase().equals(part2[j]) != true && part2[j].toUpperCase().equals(part2[i]) != true){
                n++ ;
            }
            j-- ;
        }
        if(n==0){
            return true ;
        }
        else{
            return false ;
        }
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public static char[][] dotPlot(String str1, String str2) {
        char[][] churt = new char[str2.length()][str1.length()];
        for (int i=0 ; i<str1.length(); i++) {
            for (int j=0 ; j<str2.length() ; j++) {
                churt[i][j]=' ';
            }
        }
        for (int i=0 ; i<str1.length(); i++) {
            for (int j=0 ; j<str2.length() ; j++) {
                if (str1.charAt(i) == str2.charAt(j)) {
                    churt[i][j]='*';
                }
            }
        }

        return churt ;
    }
}

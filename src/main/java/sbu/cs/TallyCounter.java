package main.java.sbu.cs;

public class TallyCounter implements sbu.cs.TallyCounterInterface {
    private int value = 0 ;

    public void count() {
        value++ ;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) throws sbu.cs.IllegalValueException {
        if(value > 9999 || value < 0){
            throw new sbu.cs.IllegalValueException() ;
        }
    }
}
